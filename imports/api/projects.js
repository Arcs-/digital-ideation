import { Mongo } from "meteor/mongo"
import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'
import SimpleSchema from "simpl-schema"

import shortid from "shortid"
shortid.characters( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~-" )

const Projects = new Mongo.Collection( "projects" )
Projects.schema = new SimpleSchema( {

	_id: {
		type: String,
		autoValue: shortid.generate
	},

	name: {
		type: String
	},

	description: {
		type: String
	},

	tags: {
		type: Array,
		optional: true
	},
	"tags.$": {
		type: String
	},

	images: {
		type: Array
	},
	"images.$": {
		type: String
	},

	// creators

	executable: {
		type: String,
		optional: true
	},

	source: {
		type: String,
		optional: true
	},

	views: {
		type: SimpleSchema.Integer,
		autoValue: _ => 0
	},

	likes: {
		type: SimpleSchema.Integer,
		autoValue: _ => 0
	},

	createdAt: {
		type: Date,
		autoValue: function() {
			return new Date()
		}
	},

	comments: {
		type: Array,
		optional: true
	},
	"comments.$": {
		type: Object
	},
	"comments.$.username": {
		type: String
	},
	"comments.$.comment": {
		type: String
	},
	"comments.$.createdAt": {
		type: Date,
		autoValue: function() {
			return new Date()
		}
	},

	history: {
		type: Array,
		optional: true
	},
	"history.$": {
		type: Object
	},
	"history.$.username": {
		type: String
	},
	"history.$.createdAt": {
		type: Date,
		autoValue: function() {
			return new Date()
		}
	}

} )

Projects.attachSchema( Projects.schema )

export { Projects }

Meteor.methods( {

	'projects.insert' ( name, description ) {
		check( text, String )
		check( description, String )

		// Make sure the user is logged in before inserting a task
		if ( !Meteor.userId() ) {
			throw new Meteor.Error( 'not-authorized' )
		}

		Projects.insert( {
			name,
			description
		} )

	},

	'projects.remove' ( id ) {
		check( id, String )

		if ( !Meteor.userId() ) {
			throw new Meteor.Error( 'not-authorized' )
		}

		Tasks.remove( id )
	},


} )