import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'
import SimpleSchema from "simpl-schema"

const Users = Meteor.users

const UserProfile = new SimpleSchema( {

} )

Users.schema = new SimpleSchema( {

	username: {
		type: String,
		// For accounts-password, either emails or username is required, but not both. It is OK to make this
		// optional here because the accounts-password package does its own validation.
		// Third-party login packages may not require either. Adjust this schema as necessary for your usage.
		optional: true
	},

	emails: {
		type: Array
		// For accounts-password, either emails or username is required, but not both. It is OK to make this
		// optional here because the accounts-password package does its own validation.
		// Third-party login packages may not require either. Adjust this schema as necessary for your usage.
	},
	"emails.$": {
		type: Object
	},
	"emails.$.address": {
		type: String,
		regEx: SimpleSchema.RegEx.Email
	},
	"emails.$.verified": {
		type: Boolean
	},

	image: {
		type: String,
		optional: true
	},

	createdAt: {
		type: Date
	},

	profile: {
		type: UserProfile,
		optional: true
	},

	services: {
		type: Object,
		optional: true,
		blackbox: true
	},

	password: {
		type: String,
		blackbox: true,
		optional: true
	},

	// Add `roles` to your schema if you use the meteor-roles package.
	// Option 1: Object type
	// If you specify that type as Object, you must also specify the
	// `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
	// Example:
	// Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP)
	// You can't mix and match adding with and without a group since
	// you will fail validation in some cases.
	roles: {
		type: Object,
		optional: true,
		blackbox: true
	},

	// In order to avoid an 'Exception in setInterval callback' from Meteor
	heartbeat: {
		type: Date,
		optional: true
	}

} )

Users.attachSchema( Users.schema )

export { Users }

if ( Meteor.isServer ) {
	Meteor.methods( {

		createNewUser( email ) {
			check( email, String )
			let userId = Accounts.createUser( { email } )
			Accounts.sendEnrollmentEmail( userId )
		},

		enrollUser( token, username, password ) {
			check( token, String )
			check( username, String )
			check( password, String )

			Accounts.setPassword( id, doc.password )
		},

		resendMail( id ) {
			check( id, String )
			Accounts.sendEnrollmentEmail( id )
		},

		tokenValid( token ) {
			check( token, String )

			const user = Meteor.users.findOne( { 'services.password.reset.token': token } )
			if ( !user ) return false

			const when = user.services.password.reset.when
			const reason = user.services.password.reset.reason
			let tokenLifetimeMs = Accounts._getPasswordResetTokenLifetimeMs()
			if ( reason === "enroll" ) {
				tokenLifetimeMs = Accounts._getPasswordEnrollTokenLifetimeMs()
			}
			const currentTimeMs = Date.now()
			if ( ( currentTimeMs - when ) > tokenLifetimeMs ) return false

			return true

		},

		deleteUser( id ) {
			// ToDo: check permission
			Users.remove( id )
		}

	} )
}