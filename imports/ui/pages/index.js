import { Template } from 'meteor/templating'
import { Projects } from '../../api/projects.js'
import { Meteor } from 'meteor/meteor'
import { Blaze } from 'meteor/blaze'

import '../components/project-detail.js'
import '../components/project.js'
import '../components/navigation.html'
import './index.html'

Template.index.helpers( {
	projects() {
		return Projects.find( {}, { sort: { createdAt: -1 } } )
	},
} )

Template.index.events( {

	'submit .new-task' ( event ) {
		// Prevent default browser form submit
		event.preventDefault()

		// Get value from form element
		const target = event.target
		const text = target.text.value

		// Insert a task into the collection
		Tasks.insert( {
			text,
			createdAt: new Date(), // current time
			owner: Meteor.userId(),
			username: Meteor.user()
				.username,
		} )

		// Clear form
		target.text.value = ''
	},
} )