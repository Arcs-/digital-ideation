import { Template } from 'meteor/templating'
import { Users } from '../../api/users.js'
import { Meteor } from 'meteor/meteor'

import '../components/user.html'
import './users.html'

Template.users.helpers( {
	users() {
		return Users.find( {} );
	},
} );

Template.users.events( {

	'click .enroll' () {

		let email = $( '' )

		Meteor.call( 'createNewUser', this._id )
	},

	'click .resend' () {
		Meteor.call( 'resendMail', this._id )
	},

	'click .delete' () {
		Meteor.call( 'deleteUser', this._id )
	},

} )