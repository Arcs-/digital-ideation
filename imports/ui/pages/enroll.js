import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import { Accounts } from 'meteor/accounts-base';
import { Users } from "../../api/users.js"

import './enroll.html'

Template.enroll.onCreated( function() {

	this.error = new ReactiveVar( "" )

	Meteor.call( 'tokenValid', Router.current().params._token, ( err, valid ) => {
		if ( !valid ) this.error.set( 'invalid token' )
	} )

} )

Template.enroll.helpers( {
	error: function() {
		return Template.instance().error.get()
	}
} )

Template.enroll.events( {
	'submit form': function( event, template ) {
		event.preventDefault()

		const token = Router.current().params._token
		const password = $( '[name=password]' ).val()

		Accounts.resetPassword( token, password, err => {
			template.error.set( err.reason )
		} )

	}
} )