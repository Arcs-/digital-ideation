import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import { Accounts } from 'meteor/accounts-base'
import { Users } from "../../api/users.js"

import './login.html'

Template.login.onCreated( function() {
	this.error = new ReactiveVar( "" )

	Meteor.call( 'createNewUser', 'patrick8@stillh.art' )

} )

Template.login.helpers( {
	error: function() {
		return Template.instance().error.get()
	}
} )


Template.login.events( {
	'submit form': function( event, template ) {
		event.preventDefault()

		var email = $( '[name=email]' ).val()
		var password = $( '[name=password]' ).val()

		Meteor.loginWithPassword( email, password, err => {
			if ( err && err.reason ) {
				template.error.set( err.reason )
				$( '[name=password]' ).val( "" )
			} else {
				Router.go( '/' )
			}
		} )

	}
} )