import { Template } from "meteor/templating"
import { Projects } from "../../api/projects.js"

import "./project.html"

Template.project.events( {
	"click .project" ( event ) {

		event.preventDefault()

		UTIL.modal.open( event.currentTarget )

		Router.go( '/project/' + this._id )

	}
} )