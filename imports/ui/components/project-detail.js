import { Template } from 'meteor/templating'
import { Projects } from '../../api/projects.js'
import { Meteor } from 'meteor/meteor'

import './project-detail.html'

Template.projectDetail.onRendered( function() {
	$( '#modal' )
		.show()
} )

Template.projectDetail.helpers( {
	project() {
		return Projects.findOne( { '_id': new Mongo.ObjectID( Router.current().params._id ) } )
	}
} )

Template.projectDetail.events( {

	'click #close' () {

		Router.go( '/' )
		UTIL.modal.close()

	},

} )