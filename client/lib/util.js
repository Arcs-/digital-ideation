const MODAL_ID = '#modal'

const UTIL = {
	modal: {

		isOpen: function() {
			return $(MODAL_ID).is(":visible")
		},

		open: function(origin) {

      const $container = $(MODAL_ID)

      if(!origin) {
        $container.css({
          width: "100vw",
          height: "100vh",
          top: 0,
          left: 0,
          display: "block",
					opacity: 1
        })
        return
      }

			const $origin = $(origin)

			$container
				.css({
					width: $origin.outerWidth(),
					height: $origin.outerHeight(),
					top: $origin.position().top - $(window).scrollTop(),
					left: $origin.position().left,
				})
				.css({
					display: "block",
					opacity: 1
				})
				.animate(
					{
						width: "100vw",
						height: "100vh",
						top: 0,
						left: 0
					},
					300,
					() => {
						$("body").css("overflow-y", "hidden")
					}
				)
		},

		close: function() {

			$("body").css("overflow-y", "scroll")
			$(MODAL_ID).animate(
				{
					top: "50vh",
					left: "50vw",
					width: 0,
					height: 0,
					opacity: -1
				},
				300,
				function() {
					$(this).hide()
				}
			)

		}
	}
}

window.UTIL = UTIL
