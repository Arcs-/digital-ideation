import './lib/util.js'

import '../imports/startup/accounts-config.js'

import './layout.html'

import '../imports/ui/pages/index.js'
import '../imports/ui/pages/about.html'
import '../imports/ui/pages/notFound.html'
import '../imports/ui/pages/links.html'
import '../imports/ui/pages/users.js'
import '../imports/ui/pages/login.js'
import '../imports/ui/pages/enroll.js'

/*
	 Template Stuff
*/
Template.registerHelper( 'formatDate', function( date ) {
	if ( !date ) return ''
	return date.toLocaleDateString( 'en-US', {
		year: 'numeric',
		month: 'short',
		day: 'numeric'
	} )
} )

Template.layout.events( {
	'click .logout': function( event ) {
		event.preventDefault()
		Meteor.logout()
	}
} )

/*
	Hotkey
*/
$( document )
	.bind( 'keyup', function( e ) {
		if ( e.key === 'Escape' && UTIL.modal.isOpen() ) {
			Router.go( '/' )
		}
	} )

/*
	Router
*/

Router.configure( {
	layoutTemplate: 'layout',
	notFoundTemplate: 'notFound'
} )


Router.route( '/', function() {
	this.render( 'index' )
	UTIL.modal.close()
} )

Router.route( '/project/:_id', {
	action: function() {
		UTIL.modal.open()
		this.render( 'projectDetail', { to: 'modal' } )
	},
	onStop: function() {
		UTIL.modal.close()
	}
} )

Router.route( '/about' )
Router.route( '/links' )

Router.route( '/users' )

Router.route( '/login' )
Router.route( '/enroll/:_token', {
	action() {
		this.render( 'enroll' )
	}
} )