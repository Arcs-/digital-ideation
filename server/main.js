import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

import '../imports/api/projects.js'
import '../imports/api/users.js'

Meteor.startup( () => {

	// setup accounts

	Accounts.urls.enrollAccount = token => Meteor.absoluteUrl( `enroll/${token}` )

	Accounts.emailTemplates.siteName = 'Digital Ideation'
	Accounts.emailTemplates.from = 'THE AI <server@digital-ideation.com>'

	Accounts.emailTemplates.enrollAccount.subject = ( user ) => {
		return `Welcome to Awesome Town`
	}

	Accounts.emailTemplates.enrollAccount.text = ( user, url ) => {
		return 'You have been selected to participate in building a better future!' +
			' To activate your account, click the link below:\n\n' +
			url
	}



} )